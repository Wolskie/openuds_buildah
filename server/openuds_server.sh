#!/bin/bash -x

server_container=$(buildah from "${1:-centos}")
script_path="$( cd "$(dirname "$0")" ; pwd -P )"
tmp=`mktemp -d`

# Create temp dir for building the 
# container
trap 'cd / && rm -fr "$tmp"' EXIT
pushd "$tmp"

# Get the code
git clone --branch v2.2 https://github.com/dkmstr/openuds

# Apply patches
git apply "$script_path/files/uds_env_db.patch" --directory openuds/

# Copy the server code and entrypoint into the container
buildah copy "$server_container" openuds/server /opt/openuds/server
buildah copy "$server_container" "$script_path/files/openuds_server_entrypoint.sh" /opt/openuds/server/entrypoint.sh

# Not sure why the requirements from the source is so incomplete
buildah copy "$server_container" "$script_path/files/requirements.txt" /opt/openuds/server/requirements.txt

# Create config file and log path
buildah run "$server_container" -- cp -ar /opt/openuds/server/src/server/settings.py.sample /opt/openuds/server/src/server/settings.py
buildah run "$server_container" -- mkdir -p /opt/openuds/server/src/log

# Install yum dependencies
buildah run "$server_container" -- yum install epel-release -y
buildah run "$server_container" -- yum update -y
buildah run "$server_container" -- yum install -y python2-pip \
	                                          python2-devel \
						  gcc \
						  gcc-c++ \
						  libxml2-devel \
						  libxslt-devel \
						  curl-devel \
						  cairo-devel \
						  openldap-devel \
						  coffee-script \
						  mariadb-devel \
						  libicu-devel \
						  mysql-connector-python

# Install their dependencies
buildah run "$server_container" -- pip install -r /opt/openuds/server/requirements.txt

# Compile messages because it takes a long time
buildah run "$server_container" -- python /opt/openuds/server/src/manage.py compilemessages


buildah run "$server_container" -- yum remove -y *-devel gcc gcc-c++
buildah run "$server_container" -- yum clean all

# Set the entrypoint
buildah config --port 8000 "$server_container"
buildah config --entrypoint "bash /opt/openuds/server/entrypoint.sh" "$server_container"
buildah commit "$server_container" "${2:-$USER/uds-server}:2.2"
