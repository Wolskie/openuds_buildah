# OpenUDS Server

Runs https://github.com/dkmstr/openuds/tree/master/server in a container.

## How to build this image.

Install dependencies.

```bash
sudo dnf install buildah podman git
```

Create the image.

```bash
sudo bash openuds_server.sh
```

## How to use the image.

### Environment Variables

Environment variables for the database connection.

* `UDS_DATABASE_NAME`
  - Name of the database to use.
* `UDS_DATABASE_USER`
  - The user name used to connect to the database.
* `UDS_DATABASE_PASSWORD`
  - The password used to connect to the database.
* `UDS_DATABASE_HOST`
  - The hostname or IP address of the database.
* `UDS_DATABASE_PORT`
  - The database port, usually 3306.

# Create a container.

An example of how to create a container.

```bash
sudo podman run --net=host --name uds-server --hostname uds-server -p 8000 \
	-e UDS_DATABASE_NAME=uds \
	-e UDS_DATABASE_USER=uds \
	-e UDS_DATABASE_PASSWORD=uds \
	-e UDS_DATABASE_HOST=127.0.0.1 \
	-e UDS_DATABASE_PORT=3306 -d uds_server:2.2
```
